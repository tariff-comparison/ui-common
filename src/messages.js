export default {
    en: {
        title: 'User Interface Common',
        sidebarTitle: 'UI Common',
        dashboard: 'Home',
        multiLevel: 'Multi Level',
        collapsed: 'Collapsed',
        rtl: 'RTL',
        image: 'Background image',
        viewSource: "Github Project",

        aboutMenu: "About",
        tariffComparison: 'Tariff Comparison',
        menuCosts: 'Costs',
        submenuCostsCalculate: 'Calculate Costs per Year',
        submenuCostsFile: 'Upload File to Calculate - Multiples',
        submenuCostsListAllCalculation: 'List All Calculations',
        menuProducts: 'Products',
        submenuProductsManage: 'Manage All Products',

    }
};
