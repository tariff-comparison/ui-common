function About() {
    return (
        <div className={'main'}>
            <h1>About</h1>
            <h2>Last updates</h2>
            <ul>
                <li>Management Products</li>
                <li>Upload Files to Process</li>
            </ul>
            <h2>Coming soon</h2>
            <ul>
                <li>All stack deployed to cloud and CI/CD</li>
                <li>Upload Multiples Files to Process</li>
                <li>Delete all processed calculations</li>
                <li>Delete many products</li>
                <li>Alerts Style Bootstrap</li>
                <li>More operations over calculations and products</li>
                <li>When creating new Product, change message to insert value month on base costs</li>
            </ul>
            <h3>Version</h3>
            <p>1.0.0-SNAPSHOT</p>
        </div>
    )
}

export default About;