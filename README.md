# UI File

User Interface File

## How To Run


1. Change Directory 
```sh
$ cd ui-react-file/
```


2. Build 
```sh
$ npm run build
```


3. Run 
```sh
$ npm run start
```

4. All the requests are listening on 3000 port. [http://localhost:3000/file-ui]